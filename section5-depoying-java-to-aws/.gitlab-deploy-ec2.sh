#!/bin/bash

# Get servers list
set -f
string=$DEPLOY_SERVER
echo $string
ssh ubuntu@$string "echo currently logged in user && whoami && echo ssh && echo to && echo ec2 && echo successful && echo from && echo gitlab pipeline "

# use commented code for deplying artifact to multiple ec2 instance
#Iterate servers for deploy and pull last commit
#for i in "${!array[@]}"; do
  #echo "Deploy project on server ${array[i]}"
  #echo -ubuntu@${array[i]}-
  #ssh -i ./ec2.pem ubuntu@${array[i]} "echo ssh-success"
#done