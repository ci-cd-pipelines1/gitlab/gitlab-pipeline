# defining default image for all job, this is called as globa image
# used this because its only 5 mb size
image: alpine:edge
# variables
variables:
  APP_VERSION: v1.0.2
  APP_EXTENSION: ".jar"
  ARTIFACT_NAME: jpe-$APP_VERSION$APP_EXTENSION # CI_PIPELINE_IID - is predefined gitlab pipeline id
                                           # this is added to uniquly identify each artifacts
                                           # refer - https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
  APP_NAME: cars-api
  OPEN_JDK_DOCKER_IMAGE: openjdk:12-alpine
  AWS_CLI_DOCKER_IMAGE: banst/awscli:1.18.76
  NEWMAN_DOCKER_IMAGE: vdespa/newman
#stages
stages:
  - compile
  - pre build
  - build
  - test
  - deploy
  - post deploy

#############################################################job syntax - start
#name-of-job:
  #stage: <stage-of-this-job>
  #image: <dockerImage>:<itag>
  #when: manual
  #allow_failure: false

  #before_script:
    #- echo before_script
  #script:
    #- echo script
  #after_script:
    #- echo after script
  #artifacts: # if we want to save files/folder from this job
    #paths: # list of paths
      #- ./target/jpe.jar # save this to gitlab artifacs which can be reused in pipeline later

#############################################################job syntax - end

#############################################################compile stage -start

compile:
  only:
    # this job will only run on creating a tag
    - tags
    # this job will only run below listed branches
    - master
    - develop
    - aws-ebs
    - aws-ec2
  stage: compile
  image:
    name: $OPEN_JDK_DOCKER_IMAGE
  script:
    - chmod +x gradlew # to resolve Permission denied on ./gradlew build
    - ./gradlew compileJava compileTest

#############################################################compile stage - end
#############################################################pre build stage - start

code quality pmd:
  only:
    # this job will only run on creating a tag
    - tags
    # this job will only run below listed branches
    - master
    - develop
    - aws-ebs
    - aws-ec2
  stage: pre build
  # below is commented as we are using same image globally
  # image: alpine:edge # used this because its only 5 mb size
  script:
    #- chmod +x gradlew
    #- ./gradlew pmdMain pmdTest
    - echo NEED-TO-ADD-COMMANDS-FOR-THIS
      #artifacts:
      #when: always
      #paths:
    #- build/reports/pmd/

#############################################################pre build stage - end
#############################################################build stage - start

build:
  stage: build
  only:
    # this job will only run on creating a tag
    - tags
    # this job will only run below listed branches
    - master
    - develop
    - aws-ebs
    - aws-ec2
  # pulls the docker image
  image: $OPEN_JDK_DOCKER_IMAGE
  script:
    - sed -i "s/CI_INFO_APP_VERSION/$APP_VERSION/" ./src/main/resources/application.properties
    - sed -i "s/CI_INFO_APP_CI_PIPELINE_IID/$CI_PIPELINE_IID/" ./src/main/resources/application.properties
    - sed -i "s/CI_INFO_APP_COMMIT/$CI_COMMIT_SHORT_SHA/" ./src/main/resources/application.properties
    - sed -i "s/CI_INFO_APP_BRANCH/$CI_COMMIT_BRANCH/" ./src/main/resources/application.properties
    # https://stackoverflow.com/questions/17668265/gradlew-permission-denied
    - chmod +x gradlew # to resolve Permission denied on ./gradlew build
    # commented since this job fail if Junit test fails, we don't want that
    #- ./gradlew build
    - ./gradlew bootJar
    # this step is needed since we added version to artifact when its deploy to EBS
    - mv ./build/libs/jpe.jar ./build/libs/$ARTIFACT_NAME
  artifacts:
    paths:
      - ./build/libs

#############################################################build stage - end
#############################################################test stage - start

smoke test:
  only:
    # this job will only run on creating a tag
    - tags
    # this job will only run below listed branches
    - master
    - develop
    - aws-ebs
    - aws-ec2
  stage: test
  image: $OPEN_JDK_DOCKER_IMAGE
  # this runs before script commands
  before_script:
    # command to install curl on alpine image
    - apk --no-cache add curl
  script:
    - java -jar ./build/libs/$ARTIFACT_NAME &
    # sleep 10 seconds
    - sleep 10 # try uncommenting this if this job fails
    - curl http://localhost:5000/actuator/health | grep "UP"

unit test:
  only:
    # this job will only run on creating a tag
    - tags
    # this job will only run below listed branches
    - master
    - develop
    - aws-ebs
    - aws-ec2
  stage: test
  image: $OPEN_JDK_DOCKER_IMAGE
  script:
    - chmod +x gradlew
    - ./gradlew test
  artifacts:
    when: always
    paths:
      - build/reports/tests/
    reports:
      junit: build/test-results/test/*.xml #this is for gitlab test result reading

#############################################################test stage - end
#############################################################deploy stage - start

deploy:
  when: manual
  only:
    # this job will only run on creating a tag
    - tags
    - aws-ebs
  stage: deploy
  image:
    name: $AWS_CLI_DOCKER_IMAGE
    # gitlab ci need it as it do not allow entry point (what is entry point in docker ?)
    entrypoint: [""]
  before_script:
    - apk --no-cache add curl
    # jq is tool for json parsing in terminal
    - apk --no-cache add jq
  script:
    # s1 - copying artifact to s3 bucket
    - aws configure  set region us-east-1
    - aws s3 cp ./build/libs/$ARTIFACT_NAME s3://$S3_BUCKET/$ARTIFACT_NAME
    # s2 - crating app version for AWS EBS
    #creating version
    - aws elasticbeanstalk create-application-version --application-name $APP_NAME --version-label $APP_VERSION --source-bundle S3Bucket=$S3_BUCKET,S3Key=$ARTIFACT_NAME
    # updating env
    # s2 - updating AWS EBS env to deploy app
    #    - below script return server url which will be used to access deployed app
    - CNAME=$(aws elasticbeanstalk update-environment --application-name $APP_NAME --environment-name "CarsApi-env" --version-label $APP_VERSION | jq '.CNAME' --raw-output)
  after_script:
    #- sleep 45
    - curl http://$CNAME/actuator/info | grep $CI_PIPELINE_IID
    - curl http://$CNAME/actuator/health | grep "UP"

deploy ec2:
  when: manual
  stage: deploy
  only:
    # this job will only run below listed branches
    - develop
    - aws-ec2
  before_script:
    - mkdir -p ~/.ssh
    # when we ssh using pem, internaly ec2 store pem as id_rsa file under ~/.ssh/ dir
    - echo -e "$SSH_PRIVATE_KEY" > ~/.ssh/id_rsa
    # next 2 commands to avoid bad permission error (how ?)
    - chmod 600 ~/.ssh/id_rsa
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
  script:
    # a separate file  tu run ssh command, places under root dir of this project
    - bash ./.gitlab-deploy-ec2.sh

#############################################################deploy stage - end
#############################################################post deploy stage - start

api test:
  only:
    # this job will only run on creating a tag
    - tags
    - aws-ebs
  stage: post deploy
  image:
    name: $NEWMAN_DOCKER_IMAGE
    entrypoint: [""]
  script:
    - newman --version
    - newman run "./postman/postman_collection.json" --environment "./postman/aws.postman_environment.json" --reporters cli,htmlextra,junit --reporter-htmlextra-export "newman/report.html" --reporter-junit-export "newman/report.xml"
  artifacts:
    when: always
    paths:
      - newman/
    reports:
      junit: newman/report.xml

#############################################################post deploy stage - end