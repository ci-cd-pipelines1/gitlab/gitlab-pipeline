package com.example.pipeline.gitlab.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("ebs")
@RestController
public class EbsController {
    @GetMapping
    public String get(){
        System.out.println("KEEPING SYSOUT IS NOT A GOOD PRACTICE");
        return "hello from get operation";
    }

    @GetMapping("map")
    public Map<String , Object> getMap(){
        Map<String , Object> map = new HashMap<>();
        map.put("id",1l);
        map.put("name","ebs");
        map.put("isErrored",false);
        return map;
    }

    @GetMapping("list")
    public List<Object> getList(){
        List<Object> list = new ArrayList<>();
        list.add("elem1");
        list.add(1);
        list.add(false);
        return list;
    }
}
