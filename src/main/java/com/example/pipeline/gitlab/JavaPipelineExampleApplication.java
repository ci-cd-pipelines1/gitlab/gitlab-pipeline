package com.example.pipeline.gitlab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaPipelineExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaPipelineExampleApplication.class, args);
    }

}
