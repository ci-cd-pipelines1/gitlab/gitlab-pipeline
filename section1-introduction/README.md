# gitlab-pipeline\section1- Introduction

## Reference
```ref
Udemy - https://www.udemy.com/course/gitlab-ci-pipelines-ci-cd-and-devops-for-beginners
gitlab - https://gitlab.com/ci-cd-pipelines1/gitlab/gitlab-pipeline.git
```

## Imporatnt file
```file
1. .gitlab-ci.yml
  - this file name should be always same as above
  - it contains congiguration for running gitlab pipeline.
```

## Imporatnt Jargon
```Jargon
1. JOB
  - job is base element of gitlab.
  - in order to run a set of commands we need to group them inside job
    -- e.g
    ```job1
      #job1
      build the car: # name of job
        script:
          - mkdir build
          - cd  build 
          - touch car.txt 
          - echo "chassis" >> car.txt 
          - echo "wheels" >> car.txt
          - echo "wheels" >> car.txt
    ```
  - Gitlab by defualut runs all the jobs parrellel unless specified using "stages".

2. STAGES
  - stages tells gitlab to group jobs
    -- e.g
    ```job1
      stages:
        - build
        - test
      #job1
      build the car: 
        stage: build # this job belongs to this stage
        script:
          - mkdir build
          - cd  build 
          - touch car.txt 
          - echo "chassis" >> car.txt 
          - echo "wheels" >> car.txt
          - echo "wheels" >> car.txt
    ```
  - Gitlab by defualut runs all the jobs parrellel unless specified using "stages".

```
